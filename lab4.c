#include <stdio.h>
#include <stdlib.h>
#include <time.h>

clock_t startClock, stopClock;

typedef struct vec
{
	int a, b, c, d;
} vec;

void fillArray(vec *array)
{
	for (int i = 0; i < 8193; i++)
	{
		array[i].a = rand() % 100 + 1;
		array[i].b = rand() % 100 + 1;
		array[i].c = rand() % 100 + 1;
		array[i].d = rand() % 100 + 1;
	}
}

float ADD_SIMD(vec arrayOne[], vec arrayTwo[], int counter)
{
	vec result;
	vec x;
	vec y;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{
			x = arrayOne[j];
			y = arrayTwo[j];

			startClock = clock();

			asm(
				"movups %1, %%xmm0\n"
				"movups %2, %%xmm1\n"
				"addps %%xmm1, %%xmm0\n"
				"movups %%xmm0, %0\n"
				: "=g"(result)
				: "g"(x), "g"(y));

			stopClock = clock();
			t += (double)(stopClock - startClock);
		}
	}
	return ((t / CLOCKS_PER_SEC) / 10);
}

float SUB_SIMD(vec arrayOne[], vec arrayTwo[], int counter)
{
	vec result;
	vec x;
	vec y;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{
			x = arrayOne[j];
			y = arrayTwo[j];

			startClock = clock();

			asm(
				"movups %1, %%xmm0\n"
				"movups %2, %%xmm1\n"
				"subps %%xmm1, %%xmm0\n"
				"movups %%xmm0, %0\n"
				: "=g"(result)
				: "g"(x), "g"(y));

			stopClock = clock();
			t += (double)(stopClock - startClock);
		}
	}
	return ((t / CLOCKS_PER_SEC) / 10);
}

float MUL_SIMD(vec arrayOne[], vec arrayTwo[], int counter)
{
	vec result;
	vec x;
	vec y;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{
			x = arrayOne[j];
			y = arrayTwo[j];

			startClock = clock();
			asm(
				"movups %1, %%xmm0\n"
				"movups %2, %%xmm1\n"
				"mulps %%xmm1, %%xmm0\n"
				"movups %%xmm0, %0\n"
				: "=g"(result)
				: "g"(x), "g"(y));

			stopClock = clock();
			t += (double)(stopClock - startClock);
		}
	}
	return ((t / CLOCKS_PER_SEC) / 10);
}

float DIV_SIMD(vec arrayOne[], vec arrayTwo[], int counter)
{
	vec result;
	vec x;
	vec y;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{
			x = arrayOne[j];
			y = arrayTwo[j];

			startClock = clock();

			asm(
				"movups %1, %%xmm0\n"
				"movups %2, %%xmm1\n"
				"divps %%xmm1, %%xmm0\n"
				"movups %%xmm0, %0\n"
				: "=g"(result)
				: "g"(x), "g"(y));

			stopClock = clock();
			t += (double)(stopClock - startClock);
		}
	}
	return ((t / CLOCKS_PER_SEC) / 10);
}

float ADD_SISD(vec arrayOne[], vec arrayTwo[], int counter)
{
	int resultA;
	int resultB;
	int resultC;
	int resultD;
	vec result;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{

			startClock = clock();

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"addl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultA)
				: "g"(arrayOne[j].a), "g"(arrayTwo[j].a));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"addl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultB)
				: "g"(arrayOne[j].b), "g"(arrayTwo[j].b));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"addl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultC)
				: "g"(arrayOne[j].c), "g"(arrayTwo[j].c));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"addl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultD)
				: "g"(arrayOne[j].d), "g"(arrayTwo[j].d));

			stopClock = clock();
			t += (double)(stopClock - startClock);

			result.a = resultA;
			result.b = resultB;
			result.c = resultC;
			result.d = resultD;
		}
	}

	return (t / CLOCKS_PER_SEC) / 10;
}

float SUB_SISD(vec arrayOne[], vec arrayTwo[], int counter)
{
	int resultA;
	int resultB;
	int resultC;
	int resultD;
	vec result;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{

			startClock = clock();

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"subl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultA)
				: "g"(arrayOne[j].a), "g"(arrayTwo[j].a));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"subl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultB)
				: "g"(arrayOne[j].b), "g"(arrayTwo[j].b));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"subl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultC)
				: "g"(arrayOne[j].c), "g"(arrayTwo[j].c));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"subl %%eax, %%ebx\n"
				"movl %%ebx, %0\n"
				: "=g"(resultD)
				: "g"(arrayOne[j].d), "g"(arrayTwo[j].d));

			stopClock = clock();
			t += (double)(stopClock - startClock);

			result.a = resultA;
			result.b = resultB;
			result.c = resultC;
			result.d = resultD;
		}
	}

	return (t / CLOCKS_PER_SEC) / 10;
}

float MUL_SISD(vec arrayOne[], vec arrayTwo[], int counter)
{
	int resultA;
	int resultB;
	int resultC;
	int resultD;
	vec result;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{

			startClock = clock();

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"mull %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultA)
				: "g"(arrayOne[j].a), "g"(arrayTwo[j].a));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"mull %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultB)
				: "g"(arrayOne[j].b), "g"(arrayTwo[j].b));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"mull %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultC)
				: "g"(arrayOne[j].c), "g"(arrayTwo[j].c));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"mull %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultD)
				: "g"(arrayOne[j].d), "g"(arrayTwo[j].d));

			stopClock = clock();
			t += (double)(stopClock - startClock);

			result.a = resultA;
			result.b = resultB;
			result.c = resultC;
			result.d = resultD;
		}
	}

	return (t / CLOCKS_PER_SEC) / 10;
}

float DIV_SISD(vec arrayOne[], vec arrayTwo[], int counter)
{
	int resultA;
	int resultB;
	int resultC;
	int resultD;
	vec result;
	double t = 0;

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < counter; j++)
		{

			startClock = clock();

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
                "xor %%edx, %%edx\n"
				"divl %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultA)
				: "g"(arrayOne[j].a), "g"(arrayTwo[j+1].a));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"xor %%edx, %%edx\n"
                "divl %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultB)
				: "g"(arrayOne[j].b), "g"(arrayTwo[j+1].b));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"xor %%edx, %%edx\n"
                "divl %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultC)
				: "g"(arrayOne[j].c), "g"(arrayTwo[j+1].c));

			asm(
				"movl %1, %%eax\n"
				"movl %2, %%ebx\n"
				"xor %%edx, %%edx\n"
                "divl %%ebx\n"
				"movl %%eax, %0\n"
				: "=g"(resultD)
				: "g"(arrayOne[j].d), "g"(arrayTwo[j+1].d));

			stopClock = clock();
			t += (double)(stopClock - startClock);

			result.a = resultA;
			result.b = resultB;
			result.c = resultC;
			result.d = resultD;
		}
	}

	return (t / CLOCKS_PER_SEC) / 10;
}

void main()
{

	// wykonania obliczeń dla 2048, 4096 i 8192 liczb
	vec arrayOne[8193];
	vec arrayTwo[8193];
	fillArray(arrayOne);
	fillArray(arrayTwo);
	srand(time(0));

	// SIMD 2048
	printf("%s\n", "Typ obliczen: SIMD");
	printf("%s\n", "Liczba liczb: 2048");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SIMD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "-", SUB_SIMD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "*", MUL_SIMD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "/", DIV_SIMD(arrayOne, arrayTwo, 2048));

	// SIMD 4096
	printf("%s\n", "Typ obliczen: SIMD");
	printf("%s\n", "Liczba liczb: 4096");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SIMD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "-", SUB_SIMD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "*", MUL_SIMD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "/", DIV_SIMD(arrayOne, arrayTwo, 4096));

	// SIMD 8192
	printf("%s\n", "Typ obliczen: SIMD");
	printf("%s\n", "Liczba liczb: 8192");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SIMD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "-", SUB_SIMD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "*", MUL_SIMD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "/", DIV_SIMD(arrayOne, arrayTwo, 8192));

	// SISD 2048
	printf("%s\n", "Typ obliczen: SISD");
	printf("%s\n", "Liczba liczb: 2048");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SISD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "-", SUB_SISD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "*", MUL_SISD(arrayOne, arrayTwo, 2048));
	printf("%s %f\n", "/", DIV_SISD(arrayOne, arrayTwo, 2048));

	// SISD 4096
	printf("%s\n", "Typ obliczen: SISD");
	printf("%s\n", "Liczba liczb: 4096");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SISD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "-", SUB_SISD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "*", MUL_SISD(arrayOne, arrayTwo, 4096));
	printf("%s %f\n", "/", DIV_SISD(arrayOne, arrayTwo, 4096));

	// SISD 8192
    printf("%s\n", "Typ obliczen: SISD");
	printf("%s\n", "Liczba liczb: 8192");
	printf("%s\n", "Średni czas [s]:");
	printf("%s %f\n", "+", ADD_SISD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "-", SUB_SISD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "*", MUL_SISD(arrayOne, arrayTwo, 8192));
	printf("%s %f\n", "/", DIV_SISD(arrayOne, arrayTwo, 8192));

}


